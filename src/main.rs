use crossterm::{
    event::{self, KeyCode, KeyEventKind},
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
    ExecutableCommand
};

use ratatui::{
    prelude::*,
    widgets::*
};
use std::{{
    io::{stdout, Result, Stdout}, char},
    env, thread, time::Duration, path::Path,
    fs::File,
    io::{BufReader, BufRead, Error}
};

pub enum Mode {
    Normal,
    Insert,
}
#[derive(PartialEq)]
pub enum MessageType {
    None,
    Error,
    Command,
}

pub struct EventManager {
    term_manager: TermManager,
    should_exit: bool,
    current_mode: Mode,
    command_buffer: CommandBuffer,
    file_manager: FileManager,
}

impl EventManager {
    pub fn init() -> Self {
        Self {
            term_manager: TermManager::init(),
            should_exit: false,
            current_mode: Mode::Normal,
            command_buffer: CommandBuffer::new(),
            file_manager: FileManager::new(),
        }
    }

    pub fn listen(&mut self) {
        if let Ok(true) = event::poll(std::time::Duration::from_millis(16)) {
            if let Ok(event::Event::Key(key)) = event::read() {
                if let MessageType::Command = self.command_buffer.message_type {
                    let _ = command_mode(key.code, &mut self.command_buffer, &mut self.should_exit);
                }
                else {
                    match &mut self.current_mode {
                        Mode::Normal => {
                            if key.kind == KeyEventKind::Press && key.code == KeyCode::Char('i') {
                                self.current_mode = Mode::Insert;
                            }
                            else if key.kind == KeyEventKind::Press && key.code == KeyCode::Char(':') {
                                    self.command_buffer.clear_message();
                                    self.command_buffer.set_message(String::new(), MessageType::Command)
                            }
                        }
                        Mode::Insert => {
                            if key.kind == KeyEventKind::Press && key.code == KeyCode::Esc {
                                self.current_mode = Mode::Normal;
                            }
                        }
                    }
                }
                self.render_term();
            }
        }
    }

    pub fn render_term(&mut self) {
        self.term_manager.render(&self.file_manager.text_buffer, &self.current_mode, &self.command_buffer.current_message, &self.command_buffer.message_type)      
    }

    pub fn event_loop(&mut self) {
        loop {
            self.listen();
            if self.should_exit {
                let _ = disable_raw_mode();
                let _ = stdout().execute(LeaveAlternateScreen);
                break;
            }
        }
    }
}

pub struct FileManager {
    text_buffer: TextBuffer,
}

impl FileManager {
    pub fn new() -> Self {
        Self {
            text_buffer: TextBuffer::new(),
        }            
    }

    pub fn load_file(&mut self, filename: &str) -> Result<()> {
        let path = Path::new(filename);
        if path.exists() {
            let file = File::open(path)?;
            let reader = BufReader::new(file);
            self.text_buffer.buffer.clear();
            for line in reader.lines() {
                let line = line?;
                self.text_buffer.add_line(&line, self.text_buffer.get_number_of_lines());
            }
            Ok(())
        } else {
        Err(Error::new(std::io::ErrorKind::NotFound, "File does not exist"))
        }
    }
}

pub fn command_mode(key: KeyCode, message_buffer: &mut CommandBuffer, should_exit: &mut bool) -> Result<()> {
    match key {
        KeyCode::Char(c) => {
            message_buffer.add_char(c);
        },
        KeyCode::Backspace | KeyCode::Delete => {
            message_buffer.delete_char();
        },
        KeyCode::Enter => {
            execute_command(message_buffer, should_exit);
        }
        _ => {}
    };
    Ok(())
}

fn execute_command(message_buffer: &mut CommandBuffer, should_exit: &mut bool) {
    let command = message_buffer.get_message();
    let _ = match command {
        "q" => *should_exit = true,
        "" => (),
        _ => {
            message_buffer.set_message(format!("no such command: '{}'", message_buffer.get_message()), MessageType::Error);
            ()
        }
    };
}

pub struct CommandBuffer {
    current_message: String,
    pub message_type: MessageType
}

impl CommandBuffer {
    pub fn new() -> Self {
        Self {
            current_message: String::new(),
            message_type: MessageType::None,
        }
    }

    pub fn set_message(&mut self, message: String, message_type: MessageType) {
        self.current_message = message;
        self.message_type = message_type;
    }

    pub fn clear_message(&mut self) {
        self.current_message.clear();
        self.message_type = MessageType::None;
    }

    pub fn get_message(&self) -> &str {
        &self.current_message
    }

    pub fn add_char(&mut self, new_char: char) {
        self.current_message.push(new_char);
    }

    pub fn delete_char(&mut self) {
        if !self.current_message.is_empty() {
            self.current_message.pop();
        }
    }
}


fn main() {
    let mut event_manager = EventManager::init();
    let args: Vec<String> = env::args().skip(1).collect();
    let filename = args.first().unwrap();
    let _ = event_manager.file_manager.load_file(filename);
    thread::sleep(Duration::from_secs(1));
    let _ = stdout().execute(EnterAlternateScreen);
    let _ = enable_raw_mode();
    event_manager.render_term();
    event_manager.event_loop();
}

struct TermManager {
    terminal: Terminal<ratatui::backend::CrosstermBackend<Stdout>>,
}
impl TermManager {
    pub fn init() -> Self {
        Self {
            terminal: Terminal::new(CrosstermBackend::new(stdout())).unwrap(),
        }
    }

    pub fn render(&mut self, text_buffer: &TextBuffer, mode: &Mode, message: &str, message_type: &MessageType) {
        let _ = self.terminal.draw(|frame| {
            let area = frame.size();
            let draw_area = Rect {
                x: 0,
                y: 0,
                height: area.height.saturating_sub(2),
                width: area.width.saturating_sub(2),
            };
            let mode_string = match mode {
                Mode::Normal => "NORMAL",
                Mode::Insert => "INSERT",
            };

            let status_bar_text = Paragraph::new(mode_string)
            .style(Style::new().white().on_black().bold());

            let status_bar = Rect {
                x: 0,
                y: area.height.saturating_sub(2),
                width: area.width,
                height: 1,
            };

            let command_buffer = Rect {
                x: 0,
                y: area.height.saturating_sub(1),
                width: area.width,
                height: 1,
            };


            let message_text = Self::format_command_buffer_string(message, message_type);
            
            let main_block = Block::new()
                .title("xen.md")
                .style(Style::new().white().on_black().bold());



            frame.render_widget(main_block, draw_area);
            frame.render_widget(status_bar_text, status_bar);
            frame.render_widget(message_text, command_buffer);
            
            let loaded_text: Text = text_buffer.clone().into();

            frame.render_widget(Paragraph::new(loaded_text), draw_area);
            
        });
    }

    fn format_command_buffer_string<'a>(message: &'a str, message_type: &'a MessageType) -> Paragraph<'a> {
        let message_string = match message_type {
            MessageType::Command => format!(":{}", message),
            _ => message.to_string(),
        };
    
        Paragraph::new(message_string)
            .style(Style::new().white().on_black().bold())
    }
    
}

#[derive(Clone)]
pub struct TextBuffer {
    buffer: Vec<String>,
}

impl TextBuffer {
    pub fn new() -> Self {
        Self {
            buffer: Vec::new(),
        }
    }

    pub fn add_line(&mut self, line: &str, line_pos: usize) {
        self.buffer.insert(line_pos, line.to_string());
    }

    pub fn remove_line(&mut self, line_pos: usize) {
        self.buffer.remove(line_pos);
    }

    pub fn add_char(&mut self, new_char: char, line_number: usize, line_pos: usize) {
        let line = &mut self.buffer[line_number];
        let mut char_vec: Vec<char> = line.chars().collect();
        char_vec.insert(line_pos, new_char);
        *line = char_vec.into_iter().collect();
    }

    pub fn get_number_of_lines(&self) -> usize {
        return self.buffer.len();
    }
}

impl From<TextBuffer> for Text<'_> {
    fn from(buffer: TextBuffer) -> Self {
        let line_max = buffer.get_number_of_lines();
        let last_index_digits = line_max.to_string().len();
        let lines: Vec<Line> = buffer.buffer
            .into_iter()
            .enumerate()
            .map(|(index, line_string)| {
                let line_num_str = format!("{:width$}", index + 1, width = last_index_digits);
                let line_with_number = format!("{} {}", line_num_str, line_string);
                Line::from(line_with_number)
            })
            .collect();
        Text::from(lines)
    }
}